# PostNord Tracking apps data model
*2019-05-07*

The dat model of the tracking apps can be devided in the following major parts

* **Trackings**
* **Accounts**
* **Settings**
* **Online shipping tool history**
* **Misc**

## Trackings

The most important and most complex part of the data model is the concpet called `Tracking`. A tracking can most easily be explained as an tracked PostNord shipment and all that comes with it. 

It concists of data from multiple data sources, both remote data fetched from PostNord systems and user data supplied by the user. Its quite heavy client side logic to update and display a tracking due to this. Requiring a lot of client side work for changes to the trackings part of the app.

### Current tracking data sources:

* **Track&Trace API**  
Shipment tracking data, this is updated in a pullToRefresh.
* **Recipientinstructions API**  
Items secret instructions, this is udpated in a pullToRefresh.
* **Flex API**  
Possible flex selections and current selection, this is updated in a pullToRefresh. Needs to be called for each tracking as we don't know if it has flex or not.
* **Destination service point API**  
The destination service point, this is updated in a pullToRefresh.
* **Service point / Depo API**  
Service point / depo data for pickup location, this is updated when app comes to foreground and in a pullToRefresh.
* **Remote config JSON**  
Contains businiuess logic based on service codes and additional services. Hosted by Bontouch. This is updated when app comes to forground.
* **User data**  
User specific data on device

### Common soon tracking data sources:

* **Return pickup API**  
Possible return pickup selections and current selection
* **Same day API**  
Called only if applicable for same day delivery

### Persisted data

The persisted data from Trackings can be divided into three groups, cached remote data, user data and secrets.

#### Cached data

* **Tracke&Trace Shipment**
* **Flex data**
* **Service point data / Depo data**
* **Destination service point data**
* **Remote config data**
* **Returns pickup data (Soon)**

#### User data

* **searchString** *String*  
  String user searched for when adding this Tracking
* **direction** *enum (incomming, outgoing)*  
  Direction for the shipment, set by getparcels or by user 
* **notification settings** *enum (allChanges, availableForDelivery, experienceFeedback, nearby, refundReminder, delivered)*   
   Individual notification settings for the tracking
* **archiveStatus** *enum (None, archived, unarchived)*  
 Used to determine if the parcel is in the archive or not
* **archiveDate** *Date*  
  Date the tracking was archived
* **dateAdded** *Date*  
   Date the trackign was added
* **lastSearched** *String*  
  Last the the tracking was searched for
* **customName** *String*  
  Custom name for the tracking
* **customSenderName** *String*  
  Custom name for the sender
* **customReceiverName** *String*  
  Custom name for the receiver
* **isManuallyMarkedAsDelivered** *Bool*  
  True if the user has marked tracking as delivered
* **hasSendExperienceFeedback** *Bool*  
  True if the user has send experience feedback

#### Secrets

* **flexCredentials** *shipmentId: String, type: String, secret: String*  
  Flex credentials added by link the user opened the app with.
* **collectCodes** *string: String, origin: enum (created, shared)*  
  Collect codes created by the user for items in the tracking.
* **recipientinstructions** *parcelboxcode: String, sesamaccessid: String, swipboxaccessdata: String*  
  Recipeint instructions for the items in the tracking. Cached from backend and added through share by other user (Pakkeboks code).

### Room for improvement

One major point for possible improvment while implementing the new backend profile would be to move as much Tracking logic as possible from the apps to backend. This would be very helpful for future development, both cost and time wise. Currently we are implementing a lot of complex logic in all clients to handle and combining all the different data sources to understand the correct state. 

Theres also a high risk of differences between implementations in the clients as they may changes differently over time and is very easy to miss in regressions.

We have very complex logic for updating trackings and determining its state. For example the logic for determining if a collect code can be created or not is very complex and done completly by the clients. Also to determine if a shipment can be picked up or not we need to combine NTT data, flex data and service point before knowing if and where it can be picked up. 

## Accounts

The `Account` concept in the apps is simply a Collo identity registration. They all share the same `DeviceId`.

### Data sources

* **Collo API**
* **Getparcels API**
* **GetParcels blacklist persitence**

### Persisted data

#### Shared for all acounts

* **DeviceId** *String (UUID)*  
  Generated once for each app installation

#### For each Account

* **key** *String*  
  Account key, email or mobile number
* **hash** *String*  
  Account hash received by backend
* **keyType** *enum (phoneNumber, email)*  
  Describes what kind of key we have
* **registerTimeout** *Date*  
  When the current unvalidated registartion times out
* **shipmentIds** *Array (String)*  
  Latest shipmentIds received by GetParcels for account
* **isLost** *Bool*  
  True if the account has been lost by a failed reclaim

### Getparcels blacklist

The getparcels blacklist is a persisted list of shipmentIds which should be ignored when found by the getparcels API. This list currently only deleted shipmentIds.


## Settings

The app constains a few app wide settings which can be set

* **Language** *enum (automatic, english, swedish, danish, norweigan, finnish)*  
  Language setting in app
* **Region** *enum (sweden, denmark, norway, finland)*  
  Region setting in app
* **Global notification settings (Incoming)** *enum (parcelAdded, allChanges, availableForDelivery, experienceFeedback, nearby, delivered)*  
   Overrides induvidual settings for incoming trackings when changed.
* **Global notification settings (Outgoing)** *enum (allChanges, delivered)*  
   Overrides induvidual settings for incoming trackings when changed.
* **Other notification settings** *enum (allChanges, delivered)*  
   Overrides induvidual settings for incoming trackings when changed.

## Online shipping tool

In the feature online shipping tool we persist data related to the purchase flow and order history.

### Sender

We persist the sender used when adding an item to the cart.

* **addressType** *enum (personal, company)*
* **company** *String*
* **organizationNumber** *String*
* **name** *String*
* **streetAddress** *String*
* **postalCode** *String*
* **countryCode** *String*
* **email** *String*

### Active cart

We persist the currently **activeCartId** *(String)* so we can fetch it when the user comes back to the app at a later time.

### Checkout information

For the checkout flow we persist the last used **contactEmail** *(String)*.

### Save card

In Online shipping tool DK we have a feature for the user to save their debit/creidt card by creating a per device AWS Cognito account. The **DeviceId** used for collo-identities is used as the user name and the user selects a password. For this we persist stubs of the saved cards on device allowing the user to list and select a saved card. 

### Order history

For the order history we persist all the information needed to view and fetch the order history for the device.

* **cartId** *String*  
   The cartId for the purchased cart
* **countryCode** *String*  
   The market it was purchased in, SE/DK
* **timestamp** *Date*  
   When it was purchased
* **minimumShipmentStatus** *enum (unknown, unused, inTransit, delivered)*  
   The Track&Trace status of the shipment in the order which has been updated the least (Not been sent being the minimum).

## Misc

Other from what has already been listed we persist a variety of misc data such like flags for shown onboardings and banners, selected location filters etc.

## Thoughts about backend profile

* What should happen to the archive on device? Migration?
* Do we still want notification settings for each Tracking?
* How should data be synced between app and backend, merging changes between multiple clients.
* It should probably be seperated in a similar way as here, meaning *trackings*, *profile + settings*, *online shipping tool*.
* ShipmentIds may change, requires logic for unregistering and registering subscritpions.
* Stubs, what should do with them? Added in app but does not exist in Track&Trace. Complex logic as duplicates may be added due to shipmentId changes.
* Refund reminder notification should handled by backend, can only be created currently if the user updates the app data at the correct time (device time).
* Should we keep support for statistics? Backend support as archived data may not be stored forever?

