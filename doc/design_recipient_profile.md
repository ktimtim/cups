

Recipient profile
-------------------




# Background
Currently all data is maintained in the app. This is information like:

- Settings, e.g. how I like to be notified
- Alias like what I call my package 
- Historical information

Introducing new channels like the web we have no place to store this information if it's not moved to a common backend space.

There is also logic that is driven by the app by calling the backend. This is logic like "does new packages exists for me" by polling the backend api. Is this a needed if we have all information about the recipient in the backend? A valid account should always have its "profile" up to date with current parcels from and to the recipient.

For more information about the current app data model please see document "PostNord Tracking apps data model.md"

# Goal
The purpose of a recipient profile are

- Store all relevant recipient data and information backend
- Shared between any device and channel
- Access is provided when authenticated through 
  - Oauth2 based IAM
  - Collo (this is lower priority)
- Process logic backend instead of client actions, e.g.
  - Detect new packages

# Architecture ideas
There are different components to take into consideration for a recipient profile.

## IAM

IAM (independent of implementation) should be responsible for manage an identity. This identity is the same independent of channel, like app or web. The IAM provide access token that are used to communicate with the backend services, like one or many profile related services. 
A access token will in the format of a JWT have the following structure:
```json
{
 "sub": "c@cho.nu",
 "email_verified": true,
 "emails": [
   {
     "verified_at": 1554380604,
     "email": "c@cho.nu"
   }
 ],
 "email": "c@cho.nu",
 "phone_number": "+46706880173",
 "user_id": "c@cho.nu",
 "scope": "openid ncp https://api.postnord.com/scopes/shipment/eventsorterservice/recipient",
 "token_type": "bearer",
 "access_token": "PFIcqLKtlxouDWL6nsK4",
 "iam_uuid": "f03efd65-015d-1f1e-afe8-98bf775baf88",
 "iss": "https://gate.ess.postnord.com",
 "exp": 1555598930,
 "iat": 1555595330
}
``` 
 From the jwt we have a unique identity of the "user", `iam_uuid`. We can also see all the verified identities for the  `iam_uuid` like emails and phones. The  `iam_uuid` is the identity we should use for the profile and the verified "identifications" the  `iam_uuid` has claimed.

## Profile model
Data related to the profile have some different characteristics. From a settings and historical perspective the profile is vary close to a "document", a document that is accessed by the `iam_uuid`. This means that all operations CRUD operations is done on the `iam_uuid`. 

> In this document we are not focusing on analytics where there could be a need to do complex queries like give me all identities that have received more than 10 parcels last month. This is more of the Profile projects scope - but not sure.

There are some  major question we have to think about when defining a model:

- How do we need to query and access the data? 
- What parts of the data are mandatory (schema) and what is free to define depending on client/channel type.

In the study we use a document db, Mongodb.

Using a document db the full "document" is accessed by the document id, iam_uuid. Documents can also be queried depending how we create indexes on the part of the document. The document is built up using sub documents.

A document is typical schema less but we can create a stricter partial control by using a json schema. A schema should be applied for all parts that are shared and independent of channel. Data should also be possible to store in the document that are specific to the channel/client. This should be schema less and its up to the client to manage the structure of the data. 

### Information areas
For the current app model please see document "PostNord Tracking apps data model.md"
#### Common
The common data model part is used by all clients and have a strict schema. The areas that are common are:

- Setting - define information like language, country, address, etc
- Notifications - define common generic notification settings
- Tracking - define the parcels information for the user. This include both ongoing and archived.
- Secrets - structure of to manage client and/or backend generated "codes". 

#### Client specific 
The client specific data model is located under `client`.
  
#### Identifications  
The 2 types of "index".

- Array of verified identifications and type
- Array of types and identifications

> This information must come from IAM of verified identifications


#### Order history
This should be stored in the Order domain using the same iam_id for access.

#### PushNotifications (app based):
Information about the push notification the user has subscribed to. This is for a recipient two type:

- Notification based on an identification like a mobile number or email
- Notification based on a shipment/itemid 

### Schema example 
Please see document "model_schema.md" for a "work in progress" how the schema could be structured.

### Data retention policy
> Should data like archived shipments ever be removed? Indexes and batch jobs
 
### Profile API
The api needs to enable access to the document, both for read and writes for the client. Some areas will only be possible to read. This is related to data structures that are created/updated by other backend services, like identifications. 
> A pam id should not even be possible to read, just indicate that one exists. 

### Rest API
A CRUD based api per sub document.
```
GET tracking
GET tracking/{id}
POST tracking/{id}
```
etc.

### "Cache replication"
App works against a local copy of the profile data and replication is managed asynchronous when internet is available. 

#### GraphQL
A graphql based api may be a better fit then a pure rest based in this case. 
Graphql demand both GET and POST
GET example would be:
    
    http://profile/graphql?query={me{setting}}
   
>  Graphql is currently uncharted territory and performance implications are not know, but since it will, initially, only be done on a single document it should not be an major issue. 

## Tracking and event automation
Our current flow to understand if new shipments is available or have new state is based on app polling, either by time interval, app state (open) or notifications.  
In the future shipment event tracking should be a pure backend flow. Subscription should be done on the accounts verified identifications. This would mean that time interval polling to register new parcels should be removed. The profile should be automatically be updated in the backend with new parcels and on events notifications should be sent out. 

We need to discuses how we should handle archive process:

- Automatic - time and event
- who should manage that - user driven or time driven - if time driven batch?
- Should we used archive to other stuff
- How long should the archived data exists.
- If archived in a different collection - if expired how to the main collection know


## Notifications for pure web users
The profile must have knowledge if a user has the app or not. 
It should be possible to have notifications as email and sms.

# New composite backend service 
TODO
Please all see "PostNord Tracking apps data model.md" for some suggestions.

# Steps

1. Just as a remote app storage. All read/write are done by app.
2. New composite service in the backend to offload app and enable web clients.

# Life cycle management
## IAM callback 
Recipient profile subscribe to IAM events:

- on create (iam_uuid)
- on verified (iam_uid + identity)
- on verified delete (iam_uid + identity)
- on account delete (iam_uid)

> This do not exists in IAM today - need a feature request
> 
## Recipient event bus 
A subscription bus that other microservices can listens on to manage their own state when changes is done in the profile.

- Redis streams?
 
## Copy on write cache
 Call back on objects write to redis cache


# Questions

- Need transitons between tracking and OST ? Or just save all - ongoing has a attribute state?
- The need to store all collect and pakkebox codes? Transient data

