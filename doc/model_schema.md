```python
schema = {"$jsonSchema":
    {
        # "$schema": "http://postnord.com/recipients#",
        "title": "JSON Schema for recipients",

        "type": "object",
        "required": ["shipments", "identifications"],

        "properties": {

            "settings": {
                "type": ["object"],
                "required": ["country", "language"],
                "properties": {
                    "country": {
                        "type": "string",
                        "description": "Country code ISO 3166-1",
                    },
                    "language": {
                        "type": "string",
                        "description": "Language code ISO 639-1",
                    },
                },
            },

            "addresses": {
                "type": ["array"],
                "minItems": 0,
                "maxItems": 5,
                "items": {
                    "required": ["country", "postal_code", "street", "street_number"],
                    "type": "object",

                    "properties": {
                        "country": {
                            "type": "string",
                            "description": "'country' must be a string and is required"
                        },
                        "city": {
                            "type": "string",
                            "description": "'country' must be a string and is required"
                        },
                        "postal_code": {
                            "type": "string",
                            "description": "'country' must be a string and is required"
                        },

                        "street": {
                            "type": "string",
                            "description": "'country' must be a string and is required"
                        },
                        "street_number": {
                            "type": "string",
                            "description": "'country' must be a string and is required"
                        },
                    },
                },
            },


            "notifcations": {
                "type": ["object"],

                "properties": {
                    "incoming": {
                        "enum": ["parcelAdded", "allChanges", "availableForDelivery", "experienceFeedback", "nearby", "delivered"],
                        "description": "Incoming notification enum",
                    },
                    "outgoing": {
                        "enum": ["allChanges", "delivered"],
                        "description": "Outgoing notification enum",
                    },
                    "other": {
                        "enum": ["allChanges", "delivered"],
                        "description": "Outgoing notification enum",
                    },
                },
            },


            "shipments": {
                "type": ["array"],
                "minItems": 0,
                "maxItems": 1000,
                "items": {
                    "required": ["id"],
                    "type": "object",

                    "properties": {
                        "id": {
                            "type": "string",
                            "description": "'id' must be a string and is required"
                        },
                        "search_string": {
                            "type": "string",
                            "description": "A string used to search for the shipmentid"
                        },
                        "direction": {
                            "enum": ["incoming", "outgoing"],
                            "description": "can only be one of the enum values and is required"
                        },
                        "alias_shipment_name": {
                            "type": "string",
                            "description": "name of the shipment"
                        },
                        "alias_sender_name":{
                            "type": "string",
                            "description": "sender name of the shipment"
                        },
                        "alias_recipien_namet":{
                            "type": "string",
                            "description": "Recipient name of the shipment"
                        },
                        "date_added":{
                            "bsonType": "long",
                             "description": "timestamp when added"
                        },
                        "date_archived":{
                            "bsonType": "long",
                            "description": "timestamp when archived"
                        },
                        "archive_status": {
                            "type": "boolean",
                            "description": "Is the shimpment archived"
                        },
                        "date_lastsearch":{
                            "bsonType": "long",
                            "description": "timestamp when last checked tracking status"
                        },
                        "sent_experiance_feedback": {
                            "type": "boolean",
                            "description": "Experiance feedback sent"
                        },
                        "marked_as_delivered": {
                            "type": "boolean",
                            "description": "Manual marked as delivered"
                        },

                    },
                },
            },
            "identifications": {
                "type": ["array"],
                "minItems": 0,
                "maxItems": 22,
                "items": {
                    "required": ["id", "type"],
                    "type": "object",

                    "properties": {
                        "id": {
                            "type": "string",
                            "description": "'id' must be a string and is required"
                        },
                        "type": {
                            "enum": ["email", "phone", "pam"],
                            "description": "can only be one of the enum values and is required"
                        }
                    },
                },

            }
        }
    }
}

def get_validation_schema_mongo():
    return schema
```
