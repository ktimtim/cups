package com.postnord.cups.util;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import com.postnord.cups.model.JwtPrincipal;

public class JwtUtil {

    public static JwtPrincipal retrieveJwtPayload() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        if (authentication instanceof OAuth2Authentication) {
            UsernamePasswordAuthenticationToken userToken = (UsernamePasswordAuthenticationToken) 
                    ((OAuth2Authentication) authentication).getUserAuthentication();
            return (JwtPrincipal) userToken.getPrincipal();
        }
        
        return null;
    }
}
