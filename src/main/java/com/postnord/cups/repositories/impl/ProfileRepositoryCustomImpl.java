package com.postnord.cups.repositories.impl;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.mongodb.client.result.UpdateResult;
import com.postnord.cups.repositories.ProfileRepositoryCustom;
import com.postnord.recipientcore.generated.Profile;
import com.postnord.recipientcore.generated.Settings;

@Component
public class ProfileRepositoryCustomImpl implements ProfileRepositoryCustom {

    private MongoTemplate mongoTemplate;
    
    public ProfileRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
    
    @Override
    public long updateSettings(String ProfileId, Settings newSettings) {
        // Create Query Object
        Query query = new Query(Criteria.where("_id").is(ProfileId));
        
        // Create Update Object
        Update update = new Update();
        update.set("settings", newSettings);
        
        UpdateResult result = mongoTemplate.updateMulti(query, update, Profile.class);
        
        return result != null ? 
                result.getModifiedCount() :
                0L;
    }

}
