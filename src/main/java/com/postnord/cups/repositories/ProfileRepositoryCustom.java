package com.postnord.cups.repositories;

import com.postnord.recipientcore.generated.Settings;

public interface ProfileRepositoryCustom {

    long updateSettings(String ProfileId,  Settings newSettings);
    
}
