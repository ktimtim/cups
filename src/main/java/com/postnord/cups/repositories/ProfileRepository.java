package com.postnord.cups.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.postnord.recipientcore.generated.Profile;
import com.postnord.recipientcore.generated.model.NotificationType;

public interface ProfileRepository extends MongoRepository<Profile, String> {
    
    // Read by ID
    List<Profile> findProfileById(String id);
    
    // Read by Embedded Document Field
    List<Profile> findProfileBySettingsCountry(String country);
    
    // Read by Multiple Embedded Document Fields
    List<Profile> findProfileBySettingsCountryAndSettingsNotificationsIncoming(String country, NotificationType incoming);

}
