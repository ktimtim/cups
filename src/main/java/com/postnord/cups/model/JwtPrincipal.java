package com.postnord.cups.model;

/**
 * POJO for JWT Payload
 * @author KTimtim
 *
 */
public class JwtPrincipal {

    private String subject;
    
    private boolean email_verified;
    
    private String email;
    
    private String iam_uuid;
    
    private String scope;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public boolean isEmail_verified() {
        return email_verified;
    }

    public void setEmail_verified(boolean email_verified) {
        this.email_verified = email_verified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIam_uuid() {
        return iam_uuid;
    }

    public void setIam_uuid(String iam_uuid) {
        this.iam_uuid = iam_uuid;
    }
    
    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return "JwtPrincipal{" + "subject='" + subject + '\'' +
                ", email_verified='" + email_verified + '\'' +
                ", email='" + email + '\'' +
                ", scope='" + scope + '\'' +
                ", iam_uuid='" + iam_uuid + '\'' + '}';
    }

}
