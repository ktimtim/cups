package com.postnord.cups.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.postnord.cups.model.JwtPrincipal;
import com.postnord.cups.service.ProfileRepositoryService;
import com.postnord.cups.util.JwtUtil;
import com.postnord.recipientcore.generated.Profile;

@RestController
public class CupsController {

    private ProfileRepositoryService profileRepositoryService;
    
    public CupsController(ProfileRepositoryService profileRepositoryService) {
        this.profileRepositoryService = profileRepositoryService;
    }

    @RequestMapping(value = { "/", "/getAllProfiles" }, 
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseEntity<?> getAllProfiles() {
        List<Profile> ProfileList = profileRepositoryService.getAllRecepientProfile();

        // ============== Sample code for accessing custom claims from JWT payload ============== 
        JwtPrincipal jwtDetails = JwtUtil.retrieveJwtPayload();
        System.out.println(jwtDetails);
        // =======================================================================================
        
        if (ProfileList != null && ProfileList.isEmpty())
            return new ResponseEntity<>(ProfileList, HttpStatus.OK);

        return new ResponseEntity<>(ProfileList, HttpStatus.BAD_REQUEST);
    }

}
