package com.postnord.cups.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.postnord.cups.repositories.ProfileRepository;
import com.postnord.cups.repositories.ProfileRepositoryCustom;
import com.postnord.recipientcore.generated.Profile;
import com.postnord.recipientcore.generated.Settings;
import com.postnord.recipientcore.generated.model.NotificationType;

@Component
public class ProfileRepositoryService {

    private ProfileRepository profileRepository;

    private ProfileRepositoryCustom profileRepositoryCustom;

    public ProfileRepositoryService(ProfileRepository profileRepository,
            ProfileRepositoryCustom profileRepositoryCustom) {
        this.profileRepository = profileRepository;
        this.profileRepositoryCustom = profileRepositoryCustom;
    }

    public List<Profile> getAllRecepientProfile() {
        return profileRepository.findAll();
    }

    public List<Profile> getRecepientProfileById(String id) {
        return profileRepository.findProfileById(id);
    }

    public List<Profile> getProfileBySettingsCountry(String country) {
        return profileRepository.findProfileBySettingsCountry(country);
    }

    public List<Profile> getProfileBySettingsCountryAndSettingsNotificationsIncoming(String country,
            NotificationType incoming) {
        return profileRepository.findProfileBySettingsCountryAndSettingsNotificationsIncoming(country,
                incoming);
    }

    public Profile saveOneProfile(Profile Profile) {
        return profileRepository.save(Profile);
    }

    public List<Profile> saveManyProfile(List<Profile> Profiles) {
        return profileRepository.saveAll(Profiles);
    }

    public long updateProfileSettings(String ProfileId, Settings newSettings) {
        return profileRepositoryCustom.updateSettings(ProfileId, newSettings);
    }

}
