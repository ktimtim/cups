package com.postnord.cups.security;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.web.util.matcher.RequestMatcher;

import com.postnord.cups.security.JwtUserAuthConverter;

/**
 * Configuration for OAuth2 Authentication using JWTs. 
 */
@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfigurer extends ResourceServerConfigurerAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(OAuth2ResourceServerConfigurer.class);

    @Value("${spring.security.oauth2.resource.jwt.keyValue}")
    private String key;

    @Value("${spring.security.oauth2.resource.jwt.audience}")
    private String jwtAudience;

    @Value("${spring.security.oauth2.resource.jwt.scopeBase}")
    private String scopeBase;

    @Value("${spring.security.oauth2.resource.jwt.scopeRecipientSuffix}")
    private String scopeRecipientSuffix;

    @Value("${spring.security.oauth2.resource.jwt.scopeAdminSuffix}")
    private String scopeAdminSuffix;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        final String cupsScope = "#oauth2.hasScope('" + scopeBase + scopeRecipientSuffix + "')";
        final String adminScope = "#oauth2.hasScope('" + scopeBase + scopeAdminSuffix + "')";

        LOG.info("Configuring OAuth2 with scopes: {} and {}", cupsScope, adminScope);

        http.requestMatcher(new BearerAuthorizationHeaderMatcher()).authorizeRequests()
                // --- Indicate API endpoints and scope access here ---
                .antMatchers("/getAllProfiles").access(cupsScope)
                .antMatchers("/").access(cupsScope)
                
                // Deny access to other endpoints
                .antMatchers("/**").denyAll();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(jwtAudience);
    }

    @Bean
    public ResourceServerTokenServices tokenServices() {
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenEnhancer(tokenEnhancer());
        defaultTokenServices.setTokenStore(tokenStore());
        return defaultTokenServices;
    }

    /**
     * Config for extracting signed JWTs. 
     * VerifierKey is used for assymetric signing, while SigningKey is for symmetric signing. 
     * @return TokenConverter object
     */
    @Bean
    public JwtAccessTokenConverter tokenEnhancer() {
        final JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        // for asymmetric key signing
        jwtAccessTokenConverter.setVerifierKey(key);
        // for symmetric key signing
        // jwtAccessTokenConverter.setSigningKey(key);
        jwtAccessTokenConverter.setAccessTokenConverter(accessTokenConverter());
        return jwtAccessTokenConverter;
    }

    private AccessTokenConverter accessTokenConverter() {
        DefaultAccessTokenConverter converter = new DefaultAccessTokenConverter();
        converter.setUserTokenConverter(new JwtUserAuthConverter(scopeBase + scopeAdminSuffix));
        return converter;
    }

    private JwtTokenStore tokenStore() {
        return new JwtTokenStore(tokenEnhancer());
    }

    /**
     * Matcher for requests with Bearer Token as Authorization header.
     */
    private class BearerAuthorizationHeaderMatcher implements RequestMatcher {
        @Override
        public boolean matches(HttpServletRequest request) {
            final String authorization = request.getHeader("Authorization");
            return authorization != null && authorization.startsWith("Bearer");
        }
    }

}
