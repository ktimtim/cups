package com.postnord.cups.security;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;

import com.google.common.collect.Sets;
import com.postnord.cups.model.JwtPrincipal;

public class JwtUserAuthConverter extends DefaultUserAuthenticationConverter {

    protected static final String CLAIM_SUBJECT = "sub";
    protected static final String CLAIM_IAM_UUID = "iam_uuid";
    protected static final String CLAIM_SCOPE = "scope";

    /** Necessary role to access actuator endpoints */
    private static final String ROLE_ACTUATOR = "ROLE_ACTUATOR";
    
    private final String adminScope;

    public JwtUserAuthConverter(final String adminScope) {
        this.adminScope = adminScope;
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        if (map.containsKey(CLAIM_SUBJECT) && map.get(CLAIM_SUBJECT) instanceof String) {
            // TODO: Add setting of other fields of JwtPrincipal here 
            JwtPrincipal principal = new JwtPrincipal();
            principal.setSubject((String) map.get(CLAIM_SUBJECT));
            if (map.containsKey(CLAIM_IAM_UUID) && map.get(CLAIM_IAM_UUID) instanceof String) {
                principal.setIam_uuid((String) map.get(CLAIM_IAM_UUID));
            }
            Set<SimpleGrantedAuthority> authorities = Sets.union(
                    getRoles(map)
                            .stream()
                            .map(SimpleGrantedAuthority::new)
                            .collect(Collectors.toSet()),
                    getScopes(map)
                            .stream()
                            .map(SimpleGrantedAuthority::new)
                            .collect(Collectors.toSet()));
            return new UsernamePasswordAuthenticationToken(principal, "N/A", authorities);
        }
        return null;
    }

    private List<String> getRoles(Map<String, ?> map) {
        return getScopes(map).stream().anyMatch(s -> adminScope.equals(s))
                ? Collections.singletonList(ROLE_ACTUATOR)
                : Collections.emptyList();
    }

    private List<String> getScopes(Map<String, ?> map) {
        return Optional.ofNullable(
                map.getOrDefault(CLAIM_SCOPE, null))
                .filter(String.class::isInstance).map(String.class::cast)
                .map(s -> s.split(" +"))
                .map(Arrays::asList)
                .orElse(Collections.emptyList());
    }

}