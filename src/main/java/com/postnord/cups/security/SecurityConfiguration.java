package com.postnord.cups.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * Configuration for setting up Authentication for endpoints.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors().and()
            .authorizeRequests()
                // -- Define authenticated endpoints here -- 
                .antMatchers("/getAllProfiles").authenticated()
                .antMatchers("/")
                .authenticated().antMatchers("/**").denyAll()
                
            // Additional security configuration
            .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // We don't need sessions to be created.
            .and().csrf().disable();
    }

}
